export const gameDesignPrototype = {
    newGameButton: window.document.getElementById("newGameButton"),
    newCardButton: window.document.getElementById("newCardButton"),
    stopButton: window.document.getElementById("stopButton"),
    cardArea: window.document.getElementById("cardArea"),
    moreDiv: window.document.getElementById("more"),
    regleDiv: window.document.getElementById("regle"),
    divGame: window.document.getElementById("gameArea"),
    divContainerbegining: window.document.getElementById("container-begining"),
    divGameStatus: window.document.getElementById("game-status"),
    restartButton: window.document.getElementById("restartButton"),
    replayButton: window.document.getElementById("replayButton"),
    bank: window.document.getElementById("bank"),
    gameStarted: window.document.getElementById("GameStarted"),
    canVibrate: window.navigator.vibrate,

    wins: document.getElementById('wins'),
    loses: document.getElementById('loses'),
    score: document.getElementById('score'),

     init(){
       console.log(document.getElementsByClassName("hidden"));
       console.log(document.getElementsByClassName("watchable"));

       Array.from(document.getElementsByClassName("hidden")).forEach(( ele) => {
        ele.classList.remove("hidden");
       })
       Array.from(document.getElementsByClassName("watchable")).forEach(( ele) => {
        ele.classList.remove("watchable");
        ele.classList.add("hidden");
       })
        this.initKnowMore();
        this.drawDeck();
        this.divGameStatus.innerHTML = "Partie en cours";
     },
     initKnowMore(){
        this.moreDiv.addEventListener('click',(event)=>{
            this.regleDiv.style.display = 'block';
            this.moreDiv.style.display = 'none';
        });
        this.regleDiv.addEventListener('click', (event)=>{
            this.moreDiv.style.display = 'block';
            this.regleDiv.style.display = 'none';
        });
    },
     drawCard(url, remaining){
        this.flipCard(url, remaining);
        let id = null;
        if (this.canVibrate) window.navigator.vibrate(200);
        this.mooveToCardArea(remaining, id);
      },
      flipCard(url, remaining){
        let imageCard= document.createElement("img");
        imageCard.src=url;
        let card = document.getElementById(remaining+1);
        imageCard.classList.add("front");
        card.appendChild(imageCard);
        card.classList.toggle('flipCard');
      },
      async mooveToCardArea(remaining, id){
        await this.sleep(500);
        let elem = document.getElementById(remaining+1).parentElement;
        let pos = 0;
        clearInterval(id);
        id = setInterval(frame, 6);
        function frame() {
          if (pos >= 75) {
            clearInterval(id);
            elem.style.zIndex = -remaining+1;
            elem.firstChild.style.marginLeft = -(remaining+1 - 52)*(50) + "px";
          } else {
            pos= pos + 1; 
            elem.style.right = pos + '%'; 
          }
        }
      },
      sleep(ms) {
        return new Promise((resolve) => {
          setTimeout(resolve, ms);
        });
      },
      drawCard2(url, remaining){
        this.flipCard(url, remaining);
        if (this.canVibrate) window.navigator.vibrate(200);

      },
      async animationEnd(win) {
        if(win){
          await this.sleep(1000);
          this.winAnimation();
          await this.sleep(4000);
        }else {
          await this.sleep(1000);
          this.looseAnimation();
          await this.sleep(1000);
        }
        this.divGameStatus.innerHTML = "Partie finie";
      },
      getScore(){
          return parseInt(this.score.textContent);
      },
      changeScore(value){
        this.score.textContent= value;
      },
      changeNbCard(value) {
        document.getElementById("remaining").innerHTML = value;
      },
      updateOnlineStatus() {
        let condition = navigator.onLine ? "online" : "offline";
        let wifiImg = document.getElementById("wifi");
        if (condition == 'online') {
          wifiImg.src = "./image/wifi.svg";
          wifiImg.alt = condition;
        } else {
          wifiImg.src = "./image/wifi-off.svg";
          wifiImg.alt = condition;
        }
    },
      drawDeck(){
        let pix = 0.25;
        for(let i = 0; i < 52; i++){
          let card = document.createElement("div");
          card.classList.add('card');
          card.style.transform = "translate("+ -pix +"px," + -pix +"px)";
          pix = pix + 0.25;
          let cardInner = document.createElement("div");
          cardInner.classList.add('card_inner');
          cardInner.id = i+1;
          card.appendChild(cardInner);
          let backCard = document.createElement("img");
          backCard.classList.add('back');
          backCard.src = './image/backCardBrown.png';
          cardInner.appendChild(backCard);
          this.bank.appendChild(card);
        }
      }, 
      winAnimation(){
        Array.from(document.getElementsByClassName("card_inner")).forEach((element)=>{
          if(element.id %2 == 0){
            element.style.animationDuration = '3s';
            element.style.animationName = 'winAnimation';
          }else{
            element.style.animationDuration = '3s';
            element.style.animationName = 'winAnimation2';

          }
        })
      },
      looseAnimation(){
        Array.from(document.getElementsByClassName("flipCard")).forEach(async (element)=>{
          element.classList.remove("flipCard");
          await this.sleep(1000);
          element.classList.add("flipCard");
        })
      },
      replayParty(){
        Array.from(this.bank.children).forEach((element) => {
          element.remove();
        });
        this.drawDeck();
        document.getElementById("replayButton").disabled = true;
        this.divGameStatus.innerHTML = "Partie en cours";
      }
      
}