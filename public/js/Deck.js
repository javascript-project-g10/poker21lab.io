export const deckPrototype ={
    _deck: [],
    _lastCard: null,
    _nbCard: 0,
    get lastCard() { 
        return this._lastCard;
    },
    set lastCard(val) {
        this._lastCard = val;
        this._deck.push(val);
    },
    get nbCard() { 
        return this._nbCard;
    },
    set nbCard(val) {
        this._nbCard = val;
    },
    getValueOfCard(card) {
        if(card == "JACK" || card == "QUEEN" || card == "KING") return 10;
        else if( card == "ACE") return 0;
        else if(isNaN(card) )return 0;
        else return parseInt(card);
    }
}