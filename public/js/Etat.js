export const etatPrototype = {
    _score: 0,
    _status: true,
    _stop: false,
    _verif: 0,
    toString() {
        return 'score: ' + this._score + ', statut: ' + this._status + '.';
    },
    get score() { return this._score; },
    set score(score) {
        if (!isNaN(score)) {
            this._score = score;
            console.log("le score est passé a " + this._score);
        }
    },
    addToScore(value) {
        this._score = this._score + value;
    },
    get status() {
        return this._status;
    },
    set status(status) {
        this._status = status;
    },
    get stop() {
        return this._stop;
    },
    set stop(stop) {
        this._stop = stop;
    },
    get verif() {
        return this._verif;
    },
    set verif(verif) {
        this._verif = verif;
    },

    isWin() {
        return this._score == 21 || (this._stop == true && this._verif > 21);
    },
    isLoose() {
        console.log(this._verif);
        return this._score > 21 || this._verif <= 21;
    },
    replayParty() {
        this._status = true;
        this._score = 0;
        this._stop = false;
        this._verif = 0;
    }
}


