import { etatPrototype } from "./Etat.js";
import { gameDesignPrototype } from "./gameDesign.js";
import { deckPrototype } from "./Deck.js";

const closeEls = document.querySelectorAll("[data-close]");
const isVisible = "is-visible";
let victoire = 0;
let perte = 0;
let scoreVerif = 0;
const canVibrate = window.navigator.vibrate;

for (const el of closeEls) {
  el.addEventListener("click", function () {
    this.parentElement.parentElement.parentElement.classList.remove(isVisible);
    document.getElementById("stopButton").disabled = true;
  });
}

document.addEventListener("click", e => {
  if (e.target == document.querySelector(".modal.is-visible")) {
    document.querySelector(".modal.is-visible").classList.remove(isVisible);
    document.getElementById("stopButton").disabled = true;
  }
});

document.addEventListener("keyup", e => {
  if (e.key == "Escape" && document.querySelector(".modal.is-visible")) {
    document.querySelector(".modal.is-visible").classList.remove(isVisible);
    document.getElementById("stopButton").disabled = true;
  }
});

let etat = Object.create(etatPrototype);
let gameDesign = Object.create(gameDesignPrototype);
let deck = Object.create(deckPrototype);
let init = null;

window.addEventListener('online', gameDesign.updateOnlineStatus);
window.addEventListener('offline', gameDesign.updateOnlineStatus);
gameDesign.updateOnlineStatus();


gameDesign.newGameButton.addEventListener('click', (event) => {
  gameDesign.init();
  init = initialise();
});

gameDesign.newCardButton.addEventListener('click', (event) => {
  if (etat.status) addCard();
});

gameDesign.stopButton.addEventListener('click', (event) => {
  etat.status = false;
  etat.verif = scoreVerif;
  verifNextCard();
});

gameDesign.restartButton.addEventListener('click', (event) => {
  restartParty();
});

gameDesign.replayButton.addEventListener('click', (event) => {
  replayParty();
});

function initialise() {
  const initial = fetch('https://deckofcardsapi.com/api/deck/new/shuffle', {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Accept": "application/json"
    }
  }).then(response => {
    if (response.ok) {
      deck.nbCard = 52;
      gameDesign.changeNbCard(deck.nbCard);
      return response.json();
    } else {
      console.log(response.status);
    }
  });
  return initial;
}

function addCard() {
  init.then(response => {
    const deckId = response.deck_id;
    fetch('https://deckofcardsapi.com/api/deck/' + deckId + '/draw/?count=1', {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        "Accept": "application/json"
      }
    }).then(response => {
      if (response.ok) {
        return response.json();
      }
    }).then(data => {
      if (data.success) {
        deck.nbCard = data.remaining;
        gameDesign.changeNbCard(deck.nbCard);
        if (etat.status) gameDesign.drawCard(data.cards[0].image, deck.nbCard);
        else {
          gameDesign.drawCard2(data.cards[0].image, deck.nbCard);
          scoreVerif = deck.getValueOfCard( data.cards[0].value);
          console.log(scoreVerif);
          scoreVerif = scoreVerif + gameDesignPrototype.getScore();
          document.getElementById("stopButton").disabled = true;
        }
        addScore(deck.getValueOfCard(data.cards[0].value));
        let nbCardsAdded = document.querySelectorAll('.front').length;
        if (nbCardsAdded >= 1) {
          document.getElementById("replayButton").disabled = false;
          document.getElementById("stopButton").disabled = false;

        }
      } else {
        alert("Fin du paquet.");
      }
    }).catch((error) => {
      alert('Une erreur est survenu.')
      console.log('error: '+ error);

    });
  });
}

function addScore(value) {
  etat.score = gameDesign.getScore();
  etat.addToScore(value);
  if (etat.score >= 21 || etat.stop == true) {
    endGame();
  }
  gameDesign.changeScore(etat.score);

}

function verifNextCard() {
  etat.stop = true;
  addCard();
  return true;
}

//draw win or lose
async function endGame(){
  etat.status=false;
  etat.verif= scoreVerif;
  console.log("etat.verif" + etat.verif);
  console.log("etat stop" + etat.stop);
  if(etat.isWin()){
    gameDesign.animationEnd(true);
    victoire++;
    console.log(victoire);
    document.getElementById("wins").innerHTML = + victoire;
    if (canVibrate) window.navigator.vibrate(200);
    await gameDesign.sleep(1000);
    document.getElementById("modal1").classList.add('is-visible');
  }else if(etat.isLoose()){
    gameDesign.animationEnd(false); 
    perte++; 
    console.log(perte);
    document.getElementById("loses").innerHTML =+ perte;
    if (canVibrate) window.navigator.vibrate(200);
    await gameDesign.sleep(1000);
    document.getElementById("modal2").classList.add('is-visible');
  }
  stopButton.setAttribute("disabled", "");
  newCardButton.setAttribute("disabled", "");
}

function restartParty() {
  window.location.href = window.location.href;
}

window.addEventListener("keydown", function (addCardByPressD) {
  switch (addCardByPressD.key) {
    case "D":
      addCard();
      break;
    default:
      return;
  }
}, true);

function replayParty() {
  etat.replayParty();
  gameDesign.changeScore(etat.score);
  gameDesign.replayParty();
  init = initialise();

  stopButton.removeAttribute("disabled");
  newCardButton.removeAttribute("disabled");
}

