# poker 21


## Installation

```
- make start
```

## Deploy

- Merge on release branch
- Visit https://javascript-project-g10.gitlab.io/poker21.gitlab.io

## Name
Poker-21

## Description
Le jeu de cartes consiste à se rapprocher le plus possible d’un score de 21.
Le joueur gagne s’il atteint 21, ou s’il décide de s’arrêter et que la carte suivante est supérieure à 21.
Chaque carte vaut sa valeur en point, sauf pour l’As qui vaut 0 point, et le roi, la reine et le valet qui valent 10 points chacun.

## Authors and acknowledgment
- Rina-Myriam Harroch
- Naty Ghanem


